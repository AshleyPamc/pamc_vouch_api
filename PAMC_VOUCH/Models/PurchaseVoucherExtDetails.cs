﻿namespace PAMC_VOUCH.Models
{
    public class PurchaseVoucherExtDetails
    {
        public string deliveryMethod { get; set; }
        public string providerId { get; set; }
        public string cutomerName { get; set; }
        public string customerSurname { get; set; }
        public string voucherBasketId { get; set; }

    }
}
