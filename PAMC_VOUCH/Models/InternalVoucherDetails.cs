﻿namespace PAMC_VOUCH.Models
{
    public class InternalVoucherDetails
    {
        public string description { get; set; }
        public string status { get; set; }
        public string voucherCategory { get; set; }
        public string voucherCost { get; set; }
        public string basketId { get; set; }
        public bool valid { get; set; }
        public string reason { get; set; }

        public string voucherCode { get; set; }


    }
}
