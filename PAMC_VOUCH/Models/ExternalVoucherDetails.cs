﻿namespace PAMC_VOUCH.Models
{
    public class ExternalVoucherDetails
    {
        public string providerId { get; set; }  
        public string voucherCode { get; set; }
    }
}
