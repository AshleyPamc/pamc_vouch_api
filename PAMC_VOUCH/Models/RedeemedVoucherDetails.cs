﻿namespace PAMC_VOUCH.Models
{
    public class RedeemedVoucherDetails
    {
        public string errorMessage { get; set; }
        public string transactionID { get; set; }
        public bool success { get; set; }
        public string customerName  { get; set; }
        public string deliveryMethod { get; set; }
        public string redeemDate { get; set; }
        public string purchaseDate { get; set; }
        public string voucherCode { get; set; }
        public string total { get; set; }
        public string voucherCategory { get; set; } 
        public string vocuherBasket { get; set; }

    }
}
