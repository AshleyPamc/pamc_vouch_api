﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace PAMC_VOUCH.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class PAMCController : ControllerBase
    {
        public static IWebHostEnvironment _env;
        public IConfiguration _configuration;
        public static string _drcConnectionString;
        public static string _portalConnectionString;
        public static string _reportConnectionString;
        public static string _RemmitanceWebSvcConnection;
        public static string _EmailWebSvcConnection;
        public static string _ReportWebSvcConnection;
        public static string _AuthWebSvcConnection;
        public static bool _allowAdmin;
        public static string _authId;
        public static string releaseDate;
        public static string _mediConnectionString;

        internal SqlConnection _drcConnection = null;
        internal SqlConnection _pamcPortalConnection = null;
        internal SqlConnection _mediConnection = null;

        public PAMCController(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
            var connections = _configuration.GetSection("ConnectionStrings").GetChildren().AsEnumerable();
            var webCon = _configuration.GetSection("WebConnections").GetChildren().AsEnumerable();
            var allowedLogin = _configuration.GetSection("AllowAdminLogin").GetChildren().AsEnumerable();
            _reportConnectionString = connections.ToArray()[1].Value;
            _drcConnectionString = connections.ToArray()[0].Value;
            _allowAdmin = Convert.ToBoolean(allowedLogin.ToArray()[0].Value);
            _portalConnectionString = connections.ToArray()[3].Value;
            _mediConnectionString = connections.ToArray()[2].Value;
            _RemmitanceWebSvcConnection = webCon.ToArray()[2].Value;
            _EmailWebSvcConnection = webCon.ToArray()[1].Value;
            _ReportWebSvcConnection = webCon.ToArray()[3].Value;
            _AuthWebSvcConnection = webCon.ToArray()[0].Value;
            this._drcConnection = new SqlConnection(_drcConnectionString);
            this._pamcPortalConnection = new SqlConnection(_portalConnectionString);
            this._mediConnection = new SqlConnection(_mediConnectionString);
            var date = _configuration.GetSection("ReleaseDate").GetChildren().AsEnumerable();
            releaseDate = date.ToArray()[0].Value.ToString();
        }

        public string DRCDatabase
        {
            get
            {
                return this._drcConnection.Database;
            }
        }
        public string PamcPortalDatabase
        {
            get
            {
                return this._pamcPortalConnection.Database;
            }
        }

        public string MediDatabase
        {
            get
            {
                return this._mediConnection.Database;
            }
        }

    }
}
