﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PAMC_VOUCH.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace PAMC_VOUCH.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class VoucherController : Base.PAMCController
    {
        public VoucherController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("Validate")]
        public InternalVoucherDetails ValidateVoucherCode([FromBody] ExternalVoucherDetails data)
        {
            InternalVoucherDetails voucherDetails = new InternalVoucherDetails();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = "";

                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);

            }
            return voucherDetails;

        }


        [HttpPost]
        [Route("Redeem")]
        public RedeemedVoucherDetails RedeemVoucher([FromBody] ExternalVoucherDetails data)
        {
            RedeemedVoucherDetails redeemedVoucher = new RedeemedVoucherDetails();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = "";

                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }
            return redeemedVoucher;
        }

        [HttpPost]
        [Route("Purchase")]
        public InternalVoucherDetails PurchaseVoucher ([FromBody] PurchaseVoucherExtDetails data)
        {
            InternalVoucherDetails voucherDetails = new InternalVoucherDetails();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    string voucherno = this.GenerateNewVoucherNo();

                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.CommandText = "";

                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return voucherDetails;
        }
        
        public string GenerateNewVoucherNo()
        {
            string voucherCode = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    Random r = new Random();
                 string vouch =   r.Next(1000000000, int.MaxValue).ToString();
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@VOUCH", vouch));

                    cmd.CommandText = "";

                    dt.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message,ex.StackTrace);
            }
            return voucherCode;
        }
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "VOUCHER";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\VOUCHER\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }
    }
}
